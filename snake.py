 # Display Image & text on I2C driven ssd1306 OLED display 
from machine import Pin, I2C, Timer
from ssd1306 import SSD1306_I2C
import framebuf
import time
import random

WIDTH  = const(128) # oled display width
HEIGHT = const(64)  # oled display height
FG_COL = const(1)
BG_COL = const(0)

DIR_NORTH = const(0)
DIR_SOUTH = const(1)
DIR_WEST  = const(2)
DIR_EAST  = const(3)

STATE_PRESS_PLAY = const(0)
STATE_RUNNING    = const(1)
STATE_GAME_OVER  = const(2)

TILE_BG = framebuf.FrameBuffer(bytearray([
    0b00000000,
    0b00000000,
    0b00000000,
    0b00000000
]), 4, 4, framebuf.MONO_HLSB)

TILE_SNAKE_HEAD = framebuf.FrameBuffer(bytearray([
    0b01100000,
    0b11110000,
    0b11110000,
    0b01100000
]), 4, 4, framebuf.MONO_HLSB)

TILE_SNAKE_BODY = framebuf.FrameBuffer(bytearray([
    0b00000000,
    0b01100000,
    0b01100000,
    0b00000000
]), 4, 4, framebuf.MONO_HLSB)

TILE_FRUIT = framebuf.FrameBuffer(bytearray([
    0b01100000,
    0b10010000,
    0b10010000,
    0b01100000
]), 4, 4, framebuf.MONO_HLSB)

TILE_BORDER_TOP = framebuf.FrameBuffer(bytearray([
    0b10010000,
    0b01010000,
    0b00110000,
    0b00010000
]), 4, 4, framebuf.MONO_HLSB)

TILE_BORDER_BOTTOM = framebuf.FrameBuffer(bytearray([
    0b10010000,
    0b10100000,
    0b11000000,
    0b10000000
]), 4, 4, framebuf.MONO_HLSB)

TILE_BORDER_LEFT = framebuf.FrameBuffer(bytearray([
    0b10000000,
    0b01000000,
    0b00100000,
    0b11110000
]), 4, 4, framebuf.MONO_HLSB)

TILE_BORDER_RIGHT = framebuf.FrameBuffer(bytearray([
    0b00010000,
    0b00100000,
    0b01000000,
    0b11110000
]), 4, 4, framebuf.MONO_HLSB)

TILE_BORDER_TOP_LEFT = framebuf.FrameBuffer(bytearray([
    0b00010000,
    0b00010000,
    0b00010000,
    0b111100000
]), 4, 4, framebuf.MONO_HLSB)

TILE_BORDER_TOP_RIGHT = framebuf.FrameBuffer(bytearray([
    0b10000000,
    0b01000000,
    0b00100000,
    0b00010000
]), 4, 4, framebuf.MONO_HLSB)

TILE_BORDER_BOTTOM_LEFT = framebuf.FrameBuffer(bytearray([
    0b10000000,
    0b01000000,
    0b00100000,
    0b00010000
]), 4, 4, framebuf.MONO_HLSB)

TILE_BORDER_BOTTOM_RIGHT = framebuf.FrameBuffer(bytearray([
    0b10010000,
    0b10100000,
    0b11000000,
    0b10000000
]), 4, 4, framebuf.MONO_HLSB)


class Pos:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def __eq__(self, other):
        return self.x == other.x and self.y == other.y
        
    def __repr__(self):
        return "Pos({}, {})".format(self.x, self.y)


class Tilemap:
    def __init__(self, framebuf):
        self.framebuf = framebuf
        self.tile_width = 4
        self.tile_height = 4
        self.width = WIDTH // self.tile_width
        self.height = HEIGHT // self.tile_height
        self.clear()

    def __repr__(self):
        print(self.map)
        
    def tile(self, pos, tile):
        self.map[pos.y][pos.x] = tile
        
    def clear(self):
        self.map = [[TILE_BG for c in range(self.width)] for r in range(self.height)]        
        
    def render(self):
        for row in range(0, self.height):
            for col in range(0, self.width):
                x = col * self.tile_width
                y = row * self.tile_height
                tile = self.map[row][col]
                self.framebuf.blit(tile, x, y)


class Playground:
    def __init__(self, tilemap):
        self.tilemap = tilemap
        self.right_border_x = self.tilemap.width - 1
        self.bottom_border_y = self.tilemap.height - 1
                
    def clear(self):
        self.tilemap.clear()
        # Draw border        
        for x in range(1, self.right_border_x):
            self.tilemap.tile(Pos(x, 0), TILE_BORDER_TOP)
            self.tilemap.tile(Pos(x, self.bottom_border_y), TILE_BORDER_BOTTOM)
        
        for y in range(1, self.bottom_border_y):
            self.tilemap.tile(Pos(0, y), TILE_BORDER_LEFT)
            self.tilemap.tile(Pos(self.right_border_x, y), TILE_BORDER_RIGHT)
        
        self.tilemap.tile(Pos(0, 0), TILE_BORDER_TOP_LEFT)    
        self.tilemap.tile(Pos(self.right_border_x, 0), TILE_BORDER_TOP_RIGHT)
        self.tilemap.tile(Pos(0, self.bottom_border_y), TILE_BORDER_BOTTOM_LEFT)    
        self.tilemap.tile(Pos(self.right_border_x, self.bottom_border_y), TILE_BORDER_BOTTOM_RIGHT)        
        
    def is_collision(self, pos):
        return (pos.x == 0 or pos.y == 0
            or pos.x == self.right_border_x or pos.y == self.bottom_border_y)
    
    def __rand_pos(self):
        return Pos(
            random.randint(1, self.right_border_x - 1),
            random.randint(1, self.bottom_border_y - 1)
        )
    
    def add_fruit(self, not_in_pos):
        found = False
        pos = self.__rand_pos()
        while pos in not_in_pos:
            pos = self.__rand_pos()
        self.fruit_pos = pos
        self.tilemap.tile(self.fruit_pos, TILE_FRUIT)
        
    def is_fruit(self, pos):
        return self.fruit_pos == pos
    

class Snake:
    def __init__(self, tilemap, x, y, length, direction):
        self.eaten = 0
        self.tilemap = tilemap
        self.direction = direction
        
        self.body = [Pos(x, y)]
        for i in range(length - 1):
            self.body.append(self.__new_pos(self.body[i]))
    
    def __new_pos(self, pos):
        x, y = pos.x, pos.y
        if self.direction == DIR_NORTH:                            
            y -= 1
        elif self.direction == DIR_SOUTH:
            y += 1
        elif self.direction == DIR_WEST:
            x -= 1
        elif self.direction == DIR_EAST:
            x += 1
        return Pos(x, y)
    
    def head(self):
        return self.body[-1]
        
    # Can't turn back
    def turn(self, direction):
        if (
            (self.direction == DIR_NORTH and direction != DIR_SOUTH)
            or (self.direction == DIR_SOUTH and direction != DIR_NORTH)
            or (self.direction == DIR_WEST and direction != DIR_EAST)
            or (self.direction == DIR_EAST and direction != DIR_WEST)
        ):
            self.direction = direction
    
    # Move the snake into the current direction
    def move(self):
        # clear old tail
        tail = self.body[0]
        self.tilemap.tile(tail, TILE_BG)

        # update body
        for i in range(0, len(self.body) - 1):
            self.body[i] = self.body[i + 1]
            self.tilemap.tile(self.body[i], TILE_SNAKE_BODY)

        # new head position
        self.body[-1] = self.__new_pos(self.body[-1])
        self.tilemap.tile(self.body[-1], TILE_SNAKE_HEAD)
            
    # Check if the head collided with the body       
    def is_collision(self):
        head = self.head()
        for i in range(0, len(self.body) - 2): # skip head
            if (self.body[i] == head):
                return True
        return False
    
    def eat(self):
        self.body.append(self.__new_pos(self.body[-1]))
        self.eaten += 1

class Game:    
    def __init__(self):       
        # Init HW
        self.button_up    = Pin(16, Pin.IN, Pin.PULL_DOWN)
        self.button_left  = Pin(13, Pin.IN, Pin.PULL_DOWN)
        self.button_down  = Pin(14, Pin.IN, Pin.PULL_DOWN)
        self.button_right = Pin(12, Pin.IN, Pin.PULL_DOWN)
        self.button_fire  = Pin(0, Pin.IN, Pin.PULL_DOWN)

        i2c = I2C(0) # Init I2C using I2C0 defaults, SCL=Pin(GP9), SDA=Pin(GP8), freq=400000
        self.display = SSD1306_I2C(WIDTH, HEIGHT, i2c)  # Init oled display
        self.display.rotate(False)

        # Define game objects
        self.tilemap = Tilemap(framebuf = self.display)
        self.playground = Playground(tilemap = self.tilemap)

        # Clear objects
        self.playground.clear()

        # Initial state
        self.state = STATE_PRESS_PLAY

    def step(self):
        if self.state == STATE_PRESS_PLAY:
            self.initial_speed = 10
            self.speed = self.initial_speed

            self.tilemap.render()
            self.display.text("Press fire", 24, 28)
            self.display.show()
            while not self.button_fire.value():
                pass
            
            self.snake = Snake(tilemap = self.tilemap, x = 3, y = 5, length = 4, direction = DIR_EAST)
            self.playground.clear()
            self.playground.add_fruit(self.snake.body)
            
            self.tilemap.render()
            self.display.show()
            self.state = STATE_RUNNING
            
        elif self.state == STATE_RUNNING:
            
            if self.button_right.value():
                self.snake.turn(DIR_EAST)
            elif self.button_left.value():
                self.snake.turn(DIR_WEST)
            elif self.button_up.value():        
                self.snake.turn(DIR_NORTH)
            elif self.button_down.value():        
                self.snake.turn(DIR_SOUTH)

            if self.speed > 0:
                self.speed -= 1
            else:               
                self.snake.move()
                self.tilemap.render()
                self.display.show()
                
                if self.playground.is_collision(self.snake.head()) or self.snake.is_collision():
                    self.state = STATE_GAME_OVER
                    
                if self.playground.is_fruit(self.snake.head()):
                    self.initial_speed = self.initial_speed * 0.75
                    self.snake.eat()
                    self.playground.add_fruit(self.snake.body)

                self.speed = self.initial_speed

        elif self.state == STATE_GAME_OVER:
            self.display.text("Game Over!", 24, 24)
            self.display.text("(score: {})".format(self.snake.eaten), 24, 32)
            self.display.show()
            time.sleep(2)
            self.playground.clear()
            self.tilemap.render()
            self.display.show()
            self.state = STATE_PRESS_PLAY
    
game = Game()

def step(time):
    game.step()

timer = Timer()
timer.init(freq=50, mode=Timer.PERIODIC, callback=step)


    